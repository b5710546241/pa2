/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */

package ku.util;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An ArrayIterator that check each element in the array 
 * used for skip null element.
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @param <T> for type-safety
 * @version 2015.02.22
 */
public class ArrayIterator<T> implements Iterator<T> {
	
	/** Collection of the objects. */
	private T[] array;
	/** Indicating the current element position. */
	private int cursor;
	
	/**
	 * Initializing the array and cursor.
	 * @param array is the collection of objects
	 */
	public ArrayIterator(T[] array){
		this.array = array;
		this.cursor = 0;
	}
	
	/**
	 * Checking whether there is the non-null element after the cursor.
	 * @return true if it meets the condition, otherwise return false
	 */
	@Override
	public boolean hasNext(){
		for(int i=cursor; i<array.length; i++){
			if(array[i] != null){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Return the non-null element after the cursor.
	 * @return the next non-null element in the array. otherwise, throws NoSuchElementException.
	 */
	@Override
	public T next(){
		if(hasNext()){
			for(int i=cursor; i<array.length; i++){
				if(array[i] != null){
					cursor++;
					return array[i];
				}
				cursor++;
			}
		}
		throw new NoSuchElementException();	
	}
	
	/**
	 * Remove most recent element returned by next() from the array by setting it to null.
	 */
	@Override
	public void remove(){
		array[cursor-1] = null;
	}
	
}
