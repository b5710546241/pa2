/*
 * This source code is Copyright 2015 by Tuangrat Mungmeerattanaworachot.
 */
package ku.util;

import java.util.EmptyStackException;

/**
 * A stack to collect the objects as 'First-in' and 'Last-out'.  
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 * @param <T> for type-safety
 * @version 2015.02.22
 */
public class Stack<T> {

	/** Collection of the objects. */
	private T[] items;
	/** Indicating the current element position. */
	private int cursor;

	/**
	 * Constructor and initial a value to class attribute.
	 * @param capacity is a capacity of stack
	 */
	public Stack(int capacity){
		if(capacity > 0)
			items = (T[]) new Object[capacity];
		else
			items = (T[]) new Object[0];
		cursor = 0;
	}

	/**
	 * Getting the maximum number of elements that this Stack can hold.
	 * @return maximum number of elements
	 */
	public int capacity(){
		if(items.length >= 0){
			return items.length;
		}
		return -1;
	}

	/**
	 * Checking for empty stack.
	 * @return true if stack is empty, otherwise false
	 */
	public boolean isEmpty(){
		if(items.length==0) return true;
		return items[0]==null;
	}

	/**
	 * Checking for full stack.
	 * @return true if stack is full, otherwise false
	 */
	public boolean isFull(){
		if(items.length==0) return true;
		if(items[items.length-1]==null)
			return false;
		return true;
	}

	/**
	 * Returning the item on the top of the stack, without removing it.
	 * @return item on the top of stack, null if stack is empty
	 */
	public T peek(){
		if(!isEmpty())
			return items[cursor-1];
		return null;
	}

	/**
	 * @see peek() and remove returned object.
	 * @return item on the top of stack
	 */
	public T pop(){
		if(!isEmpty()){			
			T topItem = peek();
			items[cursor-1] = null;
			cursor--;
			return topItem;
		}
		throw new EmptyStackException();
	}

	/**
	 * Adding new item onto the top of stack.
	 * @param obj is an object
	 */
	public void push( T obj){
		if(obj==null)
			throw new IllegalArgumentException();
			
		if(!isFull()){
			items[cursor] = obj;
			cursor++;
		}
	}

	/**
	 * Getting number of items in the stack.
	 * @return total items
	 */
	public int size(){
		return cursor;
	}
}
